## Slutrundeeeeeenn


### Rapport
  - Omskrivning
    - Purpose og Problemformulering
    - Memory afsnit skrives om til at bruge liste-index

  - Benchmarking afsnit
    - Valg af processor og GPU
    - Undersøg forskel i eksekvering
    - Tre parametre, gridsize, linecount, anglecount
    - Bottlenecks
      - memory bound
      - dda algoritme

  - Verifikation
    - Redegør for korrekthed i edge-cases
    - ingen tests
    - difficult to save correct output (experimenters regress)

  - Conclusion
  - Abstract

  - Further work
    - Testing suite
    - Implementation af Compressed Row