\begin{appendices}
    \subsection*{Code Overview}
    All the code relating to this project can be found in the \texttt{program} folder.
    Inside \texttt{program} one will find the following directories and files:
\begin{simplebox}
    \begin{verbatim}
    benchmarking/
        | cpu_vs_gpu/
            | benches.json
            | figures/
            | intersect_lib.fut
            | plotter.py
            | results/
            | runner.py
        | load_time/
            | bin/
            | data/
            | lib/
            | loadtime_plotter.py
            | make.sh
            | src/
                | bench_load_time.c
                | make_results.c
    lib/
    src/
        | intersect_lib.fut
        | intersections.fut
        | main.py
        | main.c
    make_lib.sh
    \end{verbatim}
\end{simplebox}
    We will now describe how to compile the project. Do note that the make scripts are only known to work on unix based systems. \pagebreak

    To compile the Futhark library issue the following command from the \texttt{program} directory.
    \begin{simplebox}
        \begin{verbatim}
    > sh make_lib.sh ( opencl | c | python | pyopencl) [linux]
        \end{verbatim} 
    \end{simplebox}
    This command will populate both \texttt{lib/} and \texttt{benchmarking/load\_time/lib/} with the compiled library. Since Futhark can make libraries for different targets one should always supply one of \texttt{opencl}, \texttt{c}, \texttt{python} or \texttt{pyopencl}. For the \texttt{pyopencl} build to function \texttt{pyopencl} must first be installed. We refer to \cite{PYOPENCL} for a guide on that. The \texttt{linux} flag should be supplied if the compilation takes place on a linux machine.\\

    To rerun the benchmarks relating to CPU vs GPU runtimes first compile the library as described above, then navigate to \texttt{benchmarking/cpu\_vs\_gpu/} and run the following commands:
    \begin{simplebox}
        \begin{verbatim}
    > python3 runner.py
        \end{verbatim} 
    \end{simplebox}
    This will run the benchmarks specified in \texttt{benches.json} and populate the \texttt{results/} folder.
    Now to produce a plot issue the command:
    \begin{simplebox}
        \begin{verbatim}
    > python3 plotter.py results/file.json [results/file2.json ...]
        \end{verbatim} 
    \end{simplebox}
    This will draw a plot using the data in the supplied file(s) and place it in the \texttt{figures/} folder.\\

    To run the benchmarks relating to on-the-fly calculations rather that saving and reading results first build the library and then navigate to \texttt{benchmarking/load\_time/}. Now issue the command
    \begin{simplebox}
        \begin{verbatim}
    > sh make.sh opencl [linux]
        \end{verbatim} 
    \end{simplebox}
    This will populate the \texttt{bin/} folder with the compiled binaries. As when making the library, the \texttt{linux} option should be supplied when working on a linux machine. We now need to compute the results that are to be read from disk later in the benchmark. This is done by running:
    \begin{simplebox}
        \begin{verbatim}
    > ./bin/make_results
        \end{verbatim} 
    \end{simplebox}
    This will populate the \texttt{data/} folder with the results of calling the Futhark library with some preset parameters, in binary format. Now the benchmark can be run with the command:
    \begin{simplebox}
        \begin{verbatim}
    > ./bin/bench_load_time
        \end{verbatim} 
    \end{simplebox}
    This will produce a file called \texttt{results.txt}. To plot the results in this file simply run:
    \begin{simplebox}
        \begin{verbatim}
    > python3 loadtime_plotter.py
        \end{verbatim} 
    \end{simplebox} 

    \subsection*{Usage with C}
    A brief example of how to interact with our library using C as the calling language will now be provided.
    First off, the library needs to be compiled to either the \texttt{c} or the \texttt{opencl} target. Then calling the library from a C file can be done as such:
    \begin{simplebox}
        \begin{lstlisting}[language=C]
  #include "intersections.h"
  
  /* Inside main or some other function perform the following steps:
    * 1. Initialize a configuration and a context struct for Futhark */
  struct futhark_context_config *cfg = futhark_context_config_new();
  struct futhark_context *ctx = futhark_context_new(cfg);
  
  /* 2. Initialize 3-dimensional arrays to save results in. 
    * Do note that instead of returning a single 
    * 3-dimensional array of tuples, two 3d arrays are returned. 
    * The first containing all the first entries (lengths)
    * and another with the second (indices) */
  struct futhark_f32_3d* lengths;
  struct futhark_i32_3d* indices;

  /* 3. Call the Futhark library */
  futhark_main(ctx, &lengths, &indices, grid_size, delta, line_count, scan_start, scan_end, scan_step);
        \end{lstlisting}
    \end{simplebox}
    The code shown above will properly initialize and call the futhark library. To then read the results into regular C arrays a little more work needs to be done: 
    \begin{simplebox}
        \begin{lstlisting}[language=C]
  /* 4. Allocate space for the results */
  float *lengths_arr = malloc(arr_size * sizeof(float));
  int *indices_arr   = malloc(arr_size * sizeof(float));  

  /* 5. Synchronize the context and copy results 
  * into the target arrays */
  futhark_context_sync(ctx);
  futhark_values_f32_3d(ctx, lengths, lengths_arr);
  futhark_values_i32_3d(ctx, indices, indices_arr);  

  /* 6. Free the Futhark structs */
  futhark_free_f32_3d(ctx, lengths);
  futhark_free_i32_3d(ctx, indices);

  /* 7. Free the configuration and context */
  futhark_context_free(ctx);
  futhark_context_config_free(cfg);

  /* lengths_arr and indices_arr can be worked on from here */
        \end{lstlisting}
    \end{simplebox}
    An example of this interaction can be found in \texttt{benchmarking/load\_time/src/make\_results.c}.

    \subsection*{Usage with Python}
    The library can also be called from Python. To do this first compile the library to either the \texttt{python} or \texttt{pyopencl} target. The following code illustrates how to use the library with Python:
    \begin{simplebox}
      \begin{lstlisting}[language=Python]
  from intersections import intersections

  # Instantiate an instance of the library object
  lib_hook = intersections()
  
  # Call the entry-point function of the library 
  result = lib_hook.main(grid_size, delta, line_count, scan_start, scan_end, scan_step)

  # Retrieve the results
  lengths = result.data[0]
  indices = result.data[1]
      \end{lstlisting}  
    \end{simplebox}

    \subsection*{Futhark Matrix Multiplication}
    \begin{simplebox}
    \begin{lstlisting}
  let mat_mult(x: [n][m]f32, y: [m][p]f32): [n][p]f32 =
      map (\xr -> map (\yc -> reduce (+) 0f32 (zipWith (*) xr yc))
                          (transpose y))
          x
    \end{lstlisting}
    \end{simplebox}

    \subsection*{Futhark Vector Subtraction}
    \begin{simplebox}
    \begin{lstlisting}
  let vec_sub(x: [d]f32) (y: [d]f32): [d]f32 =
      map (+) x y
    \end{lstlisting}
    \end{simplebox}

    \subsection*{Futhark Vector Length}
    \begin{simplebox}
    \begin{lstlisting}
  let vec_len(x: [d]f32) : f32 =
      f32.sqrt( reduce (+) 0f32 (map (\v -> v**2f32) x) )
    \end{lstlisting}
    \end{simplebox}

\end{appendices}